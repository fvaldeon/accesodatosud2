package com.fvaldeon.aplicacion1bbdd.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Created by Profesor on 23/11/2017.
 */
public class Controlador implements ActionListener{
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;

        addActionListeners(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "salir":
                System.exit(0);
                break;

            case "datosConexion":

                DialogoConexion dialogo = new DialogoConexion();
                int resultado = dialogo.mostrarDialogo();
                if(resultado == DialogoConexion.ACEPTAR){
                    try {
                        modelo.conectar(dialogo.getSgbd(), dialogo.getBbdd(),
                                dialogo.getServidor(), dialogo.getPuerto(),
                                dialogo.getUsuario(), dialogo.getPassword());
                        JOptionPane.showMessageDialog(null,"Conexion establecida");
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Error en la conexion",
                                "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;

            case "Ejecutar":
                try {
                    mostrarDatos(modelo.consultaSeleccion(vista.txtAConsulta.getText()));
                } catch (SQLException e1) {
                    JOptionPane.showMessageDialog(null, "Error en la ejecución de la consulta",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
                break;
        }
    }


    private void addActionListeners(ActionListener listener) {
        vista.itemSalir.addActionListener(this);
        vista.btnEjecutar.addActionListener(this);
        vista.itemDatosConexion.addActionListener(this);
    }


    private void mostrarDatos(ResultSet resultado) throws SQLException {
        Object[] fila;
        ResultSetMetaData metaDatos = resultado.getMetaData();
        int numColumnas = metaDatos.getColumnCount();

        fila = new Object[numColumnas];

        for (int i = 1 ; i <= numColumnas; i++){
            System.out.println(metaDatos.getColumnName(i));
            fila[i-1] = metaDatos.getColumnName(i);
        }
        vista.dtm.setColumnIdentifiers(fila);

        vista.dtm.setRowCount(0);

        while(resultado.next()) {

            for (int i = 1; i <= numColumnas; i++) {
                fila[i-1] = resultado.getString(i);
            }
            vista.dtm.addRow(fila);
        }
    }

}

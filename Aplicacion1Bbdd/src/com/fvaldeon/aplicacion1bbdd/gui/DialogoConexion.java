package com.fvaldeon.aplicacion1bbdd.gui;

import javax.swing.*;
import java.awt.event.*;

public class DialogoConexion extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtBbdd;
    private JTextField txtServidor;
    private JTextField txtPuerto;
    private JTextField txtUsuario;
    private JPasswordField passwordField;
    private JComboBox cBoxSgdb;

    private int estado;

    public final static int ACEPTAR = 1;
    public final static int CANCELAR = 0;

    private String sgbd;
    private String bbdd;
    private String servidor;
    private String puerto;
    private String usuario;
    private String password;

    public String getSgbd() {
        return sgbd;
    }

    public String getBbdd() {
        return bbdd;
    }

    public String getServidor() {
        return servidor;
    }

    public String getPuerto() {
        return puerto;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPassword() {
        return password;
    }

    public DialogoConexion() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setLocationRelativeTo(null);
    }

    private void onOK() {
        // add your code here
        this.bbdd = txtBbdd.getText();
        this.password = String.valueOf(passwordField.getPassword());
        this.usuario = txtUsuario.getText();
        this.puerto = txtPuerto.getText();
        this.servidor = txtServidor.getText();
        this.sgbd = (String)cBoxSgdb.getSelectedItem();

        estado = ACEPTAR;
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        estado = CANCELAR;
        dispose();
    }

    public int mostrarDialogo(){
        setVisible(true);
        return estado;
    }
}

package com.fvaldeon.aplicacion1bbdd.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by Profesor on 23/11/2017.
 */
public class Vista extends JFrame{
    private JPanel panel1;
    JTextArea txtAConsulta;
    JButton btnEjecutar;
    JTable tabla;

    JMenuItem itemSalir;
    JMenuItem itemDatosConexion;

    DefaultTableModel dtm;

    public Vista() {
        setTitle("Aplicacion Bbdd");
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        setLocationRelativeTo(null);
        pack();
        setVisible(true);

        dtm = new DefaultTableModel();
        tabla.setModel(dtm);
    }

    private void crearMenu(){
        itemDatosConexion = new JMenuItem("Datos Conexion");
        itemDatosConexion.setActionCommand("datosConexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemDatosConexion);
        menuArchivo.add(itemSalir);

        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        setJMenuBar(barraMenu);
    }

}

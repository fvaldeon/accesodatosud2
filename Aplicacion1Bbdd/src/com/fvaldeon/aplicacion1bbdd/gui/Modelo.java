package com.fvaldeon.aplicacion1bbdd.gui;

import java.sql.*;

/**
 * Created by Profesor on 23/11/2017.
 */
public class Modelo {
    private Connection conexion;

    public void conectar(String sgbd, String bbdd, String servidor,
                         String puerto, String usuario, String password) throws SQLException {

        String parametrosConexion = "jdbc:" + sgbd + "://" + servidor + ":"
                + puerto + "/" + bbdd;
        conexion = DriverManager.getConnection(parametrosConexion, usuario, password);
    }

    public void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }

    public ResultSet consultaSeleccion(String consulta) throws SQLException {
        ResultSet resultado = null;
        PreparedStatement sentencia = null;

        sentencia = conexion.prepareStatement(consulta);
        resultado = sentencia.executeQuery();

        return resultado;
    }

}

import com.fvaldeon.aplicacion1bbdd.gui.Controlador;
import com.fvaldeon.aplicacion1bbdd.gui.Modelo;
import com.fvaldeon.aplicacion1bbdd.gui.Vista;

/**
 * Created by Profesor on 23/11/2017.
 */
public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();

        Controlador controlador = new Controlador(vista,modelo);
        }

    }

